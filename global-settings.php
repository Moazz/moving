<?php
include 'wp-load.php';
// stop the page if the user not coming from wp admin
if ( ! is_user_logged_in() ) {
	echo "please login first";
	echo "<br>";
	echo '<a href="' . esc_url( admin_url() ) . '">' . esc_html( 'Wp admin' ) . '</a>';
	exit;
}

if ( isset( $_POST ) && isset( $_POST['update-colors'] ) ) {
	
	$colors = $_POST['colors'];
	
	$theme_json = file_exists( get_template_directory() . '/theme.json' ) ? file_get_contents( get_stylesheet_directory_uri() . '/theme.json' ) : '';
	
	if ( $theme_json ) {
		$theme_json_array    = json_decode( $theme_json, true );
		$theme_color_pallets = $theme_json_array['settings']['color']['palette'];
		foreach ( $theme_color_pallets as $index => $color ) {
			$theme_json_array['settings']['color']['palette'][ $index ]['color'] = $colors[ $index ];
		}
		
		unlink( get_template_directory() . '/theme.json' );
		$new_theme_json = fopen( get_template_directory() . "/theme.json", "w" ) or die( "Unable to open file!" );
		fwrite( $new_theme_json, json_encode( $theme_json_array ) );
		fclose( $new_theme_json );
		
		// backup
		$theme_json_backup = fopen( get_template_directory() . "/theme-backup.json", "w" ) or die( "Unable to open file!" );
		fwrite( $theme_json_backup, $theme_json );
		fclose( $theme_json_backup );
		
	}
	header( "Location: " . $_SERVER['PHP_SELF'] );
}

if ( class_exists( 'WP_Theme_JSON_Resolver' ) ) {
	$settings      = WP_Theme_JSON_Resolver::get_theme_data()->get_settings();
	$colors        = \Theme\Helpers::get_key_from_array( 'color', $settings );
	$color_pallets = \Theme\Helpers::get_key_from_array( 'palette', $colors ) ? $colors['palette']['theme'] : '';
}
?>

<!doctype html>
<html lang="en" class="bg-color">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
  <title>Global settings</title>
  <style>
    * {
      box-sizing: border-box;
      padding: 0;
      margin: 0;
    }
    
    body {
      font-family: "Open Sans", sans-serif;
      color: #50555a;
      padding: 100px 20px;
    }
    
    nav {
      z-index: 9;
      border-bottom: 1px solid rgba(0, 0, 0, 0.1);
      color: white;
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      padding: 20px 0;
      text-align: center;
    }
    
    .bg-color {
      background-color: #46a1de;
      transition-duration: 0.2s;
    }
    
    .text-color {
      color: #46a1de;
      transition-duration: 0.2s;
    }
    
    footer {
      padding: 40px 0;
      text-align: center;
      opacity: 0.33;
      color: white;
    }
    
    .wrapper {
      min-width: 600px;
      max-width: 900px;
      margin: 0 auto;
    }
    
    .tabs {
      display: table;
      table-layout: fixed;
      width: 100%;
      -webkit-transform: translateY(5px);
      transform: translateY(5px);
    }
    
    .tabs > li {
      transition-duration: 0.25s;
      display: table-cell;
      list-style: none;
      text-align: center;
      padding: 20px 20px 25px 20px;
      position: relative;
      overflow: hidden;
      cursor: pointer;
      color: white;
    }
    
    .tabs > li:before {
      z-index: -1;
      position: absolute;
      content: "";
      width: 100%;
      height: 120%;
      top: 0;
      left: 0;
      background-color: rgba(255, 255, 255, 0.3);
      -webkit-transform: translateY(100%);
      transform: translateY(100%);
      transition-duration: 0.25s;
      border-radius: 5px 5px 0 0;
    }
    
    .tabs > li:hover:before {
      -webkit-transform: translateY(70%);
      transform: translateY(70%);
    }
    
    .tabs > li.active {
      color: #50555a;
    }
    
    .tabs > li.active:before {
      transition-duration: 0.2s;
      background-color: white;
      -webkit-transform: translateY(0);
      transform: translateY(0);
    }
    
    .tab__content {
      background-color: white;
      position: relative;
      width: 100%;
      border-radius: 5px;
    }
    
    .tab__content > li {
      width: 100%;
      position: absolute;
      top: 0;
      left: 0;
      display: none;
      list-style: none;
    }
    
    .tab__content > li .content__wrapper {
      text-align: center;
      border-radius: 5px;
      width: 100%;
      padding: 45px 40px 40px 40px;
      background-color: white;
    }
    
    .content__wrapper h2 {
      width: 100%;
      text-align: center;
      padding-bottom: 20px;
      font-weight: 300;
    }
    
    .colors {
      text-align: center;
      padding-top: 20px;
    }
    
    .colors > li {
      list-style: none;
      width: 50px;
      height: 50px;
      border-radius: 50%;
      border-bottom: 5px solid rgba(0, 0, 0, 0.1);
      display: inline-block;
      margin: 0 10px;
      cursor: pointer;
      transition-duration: 0.2s;
      box-shadow: 0 2px 1px rgba(0, 0, 0, 0.2);
    }
    
    .colors > li:hover {
      -webkit-transform: scale(1.2);
      transform: scale(1.2);
      border-bottom: 10px solid rgba(0, 0, 0, 0.15);
      box-shadow: 0 10px 10px rgba(0, 0, 0, 0.2);
    }
    
    .colors > li.active-color {
      -webkit-transform: scale(1.2) translateY(-10px);
      transform: scale(1.2) translateY(-10px);
      box-shadow: 0 10px 10px rgba(0, 0, 0, 0.2);
      border-bottom: 20px solid rgba(0, 0, 0, 0.15);
    }
    
    .colors > li:nth-child(1) {
      background-color: #2ecc71;
    }
    
    .colors > li:nth-child(2) {
      background-color: #D64A4B;
    }
    
    .colors > li:nth-child(3) {
      background-color: #8e44ad;
    }
    
    .colors > li:nth-child(4) {
      background-color: #46a1de;
    }
    
    .colors > li:nth-child(5) {
      background-color: #bdc3c7;
    }
    
    #colors_container {
      display: flex;
      flex-wrap: wrap;
    }
    
    #colors_container button.color {
      width: 40px;
      height: 40px;
    }
    
    .colors-wrapper {
      display: flex;
      justify-content: space-between;
    }
    
    .colors-wrapper form {
      width: 40%;
    }
    
    .colors-wrapper form label {
      display: block;
      width: fit-content;
      margin-bottom: 40px;
    }
    
    .colors-wrapper form input[type="submit"] {
      background-color: #46a1de;
      padding: 10px;
      font-size: 20px;
      color: #FFF;
      cursor: pointer;
      border: 0;
    }
    
    .colors-wrapper #colors_container {
      width: 55%;
    }
  </style>
</head>
<body>
<nav class="bg-color">Global Settings & Styles</nav>
<section class="wrapper">
  <ul class="tabs">
    <li class="active">Colors</li>
    <li>Favorite movies</li>
    <li>About</li>
  </ul>
  <ul class="tab__content">
    <li class="active">
      <div class="content__wrapper">
        <div class="colors-wrapper">
			<?php if ( isset( $color_pallets ) && is_array( $color_pallets ) && ! empty( $color_pallets ) ): ?>
              <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>">
				  <?php foreach ( $color_pallets as $color ): ?>
                    <label>
						<?= $color['name'] ?>
                      <input type="color" name="colors[]" colorformat="rgba" value="<?= $color['color'] ?>">
                    </label>
				  <?php endforeach; ?>
                <input type="submit" value="Update" name="update-colors">
              </form>
              <div id="colors_container" class="row">
                <button class="color" style="background: rgb(255, 235, 238);"></button>
                <button class="color" style="background: rgb(255, 205, 210);"></button>
                <button class="color" style="background: rgb(239, 154, 154);"></button>
                <button class="color" style="background: rgb(229, 115, 115);"></button>
                <button class="color" style="background: rgb(239, 83, 80);"></button>
                <button class="color" style="background: rgb(244, 67, 54);"></button>
                <button class="color" style="background: rgb(229, 57, 53);"></button>
                <button class="color" style="background: rgb(211, 47, 47);"></button>
                <button class="color" style="background: rgb(198, 40, 40);"></button>
                <button class="color" style="background: rgb(183, 28, 28);"></button>
                <button class="color" style="background: rgb(252, 228, 236);"></button>
                <button class="color" style="background: rgb(248, 187, 208);"></button>
                <button class="color" style="background: rgb(244, 143, 177);"></button>
                <button class="color" style="background: rgb(240, 98, 146);"></button>
                <button class="color" style="background: rgb(236, 64, 122);"></button>
                <button class="color" style="background: rgb(233, 30, 99);"></button>
                <button class="color" style="background: rgb(216, 27, 96);"></button>
                <button class="color" style="background: rgb(194, 24, 91);"></button>
                <button class="color" style="background: rgb(173, 20, 87);"></button>
                <button class="color" style="background: rgb(136, 14, 79);"></button>
                <button class="color" style="background: rgb(243, 229, 245);"></button>
                <button class="color" style="background: rgb(225, 190, 231);"></button>
                <button class="color" style="background: rgb(206, 147, 216);"></button>
                <button class="color" style="background: rgb(186, 104, 200);"></button>
                <button class="color" style="background: rgb(171, 71, 188);"></button>
                <button class="color" style="background: rgb(156, 39, 176);"></button>
                <button class="color" style="background: rgb(142, 36, 170);"></button>
                <button class="color" style="background: rgb(123, 31, 162);"></button>
                <button class="color" style="background: rgb(106, 27, 154);"></button>
                <button class="color" style="background: rgb(74, 20, 140);"></button>
                <button class="color" style="background: rgb(237, 231, 246);"></button>
                <button class="color" style="background: rgb(209, 196, 233);"></button>
                <button class="color" style="background: rgb(179, 157, 219);"></button>
                <button class="color" style="background: rgb(149, 117, 205);"></button>
                <button class="color" style="background: rgb(126, 87, 194);"></button>
                <button class="color" style="background: rgb(103, 58, 183);"></button>
                <button class="color" style="background: rgb(94, 53, 177);"></button>
                <button class="color" style="background: rgb(81, 45, 168);"></button>
                <button class="color" style="background: rgb(69, 39, 160);"></button>
                <button class="color" style="background: rgb(49, 27, 146);"></button>
                <button class="color" style="background: rgb(232, 234, 246);"></button>
                <button class="color" style="background: rgb(197, 202, 233);"></button>
                <button class="color" style="background: rgb(159, 168, 218);"></button>
                <button class="color" style="background: rgb(121, 134, 203);"></button>
                <button class="color" style="background: rgb(92, 107, 192);"></button>
                <button class="color" style="background: rgb(63, 81, 181);"></button>
                <button class="color" style="background: rgb(57, 73, 171);"></button>
                <button class="color" style="background: rgb(48, 63, 159);"></button>
                <button class="color" style="background: rgb(40, 53, 147);"></button>
                <button class="color" style="background: rgb(26, 35, 126);"></button>
                <button class="color" style="background: rgb(227, 242, 253);"></button>
                <button class="color" style="background: rgb(187, 222, 251);"></button>
                <button class="color" style="background: rgb(144, 202, 249);"></button>
                <button class="color" style="background: rgb(100, 181, 246);"></button>
                <button class="color" style="background: rgb(66, 165, 245);"></button>
                <button class="color" style="background: rgb(33, 150, 243);"></button>
                <button class="color" style="background: rgb(30, 136, 229);"></button>
                <button class="color" style="background: rgb(25, 118, 210);"></button>
                <button class="color" style="background: rgb(21, 101, 192);"></button>
                <button class="color" style="background: rgb(13, 71, 161);"></button>
                <button class="color" style="background: rgb(225, 245, 254);"></button>
                <button class="color" style="background: rgb(179, 229, 252);"></button>
                <button class="color" style="background: rgb(129, 212, 250);"></button>
                <button class="color" style="background: rgb(79, 195, 247);"></button>
                <button class="color" style="background: rgb(41, 182, 246);"></button>
                <button class="color" style="background: rgb(3, 169, 244);"></button>
                <button class="color" style="background: rgb(3, 155, 229);"></button>
                <button class="color" style="background: rgb(2, 136, 209);"></button>
                <button class="color" style="background: rgb(2, 119, 189);"></button>
                <button class="color" style="background: rgb(1, 87, 155);"></button>
                <button class="color" style="background: rgb(224, 247, 250);"></button>
                <button class="color" style="background: rgb(178, 235, 242);"></button>
                <button class="color" style="background: rgb(128, 222, 234);"></button>
                <button class="color" style="background: rgb(77, 208, 225);"></button>
                <button class="color" style="background: rgb(38, 198, 218);"></button>
                <button class="color" style="background: rgb(0, 188, 212);"></button>
                <button class="color" style="background: rgb(0, 172, 193);"></button>
                <button class="color" style="background: rgb(0, 151, 167);"></button>
                <button class="color" style="background: rgb(0, 131, 143);"></button>
                <button class="color" style="background: rgb(0, 96, 100);"></button>
                <button class="color" style="background: rgb(224, 242, 241);"></button>
                <button class="color" style="background: rgb(178, 223, 219);"></button>
                <button class="color" style="background: rgb(128, 203, 196);"></button>
                <button class="color" style="background: rgb(77, 182, 172);"></button>
                <button class="color" style="background: rgb(38, 166, 154);"></button>
                <button class="color" style="background: rgb(0, 150, 136);"></button>
                <button class="color" style="background: rgb(0, 137, 123);"></button>
                <button class="color" style="background: rgb(0, 121, 107);"></button>
                <button class="color" style="background: rgb(0, 105, 92);"></button>
                <button class="color" style="background: rgb(0, 77, 64);"></button>
                <button class="color" style="background: rgb(232, 245, 233);"></button>
                <button class="color" style="background: rgb(200, 230, 201);"></button>
                <button class="color" style="background: rgb(165, 214, 167);"></button>
                <button class="color" style="background: rgb(129, 199, 132);"></button>
                <button class="color" style="background: rgb(102, 187, 106);"></button>
                <button class="color" style="background: rgb(76, 175, 80);"></button>
                <button class="color" style="background: rgb(67, 160, 71);"></button>
                <button class="color" style="background: rgb(56, 142, 60);"></button>
                <button class="color" style="background: rgb(46, 125, 50);"></button>
                <button class="color" style="background: rgb(27, 94, 32);"></button>
                <button class="color" style="background: rgb(241, 248, 233);"></button>
                <button class="color" style="background: rgb(220, 237, 200);"></button>
                <button class="color" style="background: rgb(197, 225, 165);"></button>
                <button class="color" style="background: rgb(174, 213, 129);"></button>
                <button class="color" style="background: rgb(156, 204, 101);"></button>
                <button class="color" style="background: rgb(139, 195, 74);"></button>
                <button class="color" style="background: rgb(124, 179, 66);"></button>
                <button class="color" style="background: rgb(104, 159, 56);"></button>
                <button class="color" style="background: rgb(85, 139, 47);"></button>
                <button class="color" style="background: rgb(51, 105, 30);"></button>
                <button class="color" style="background: rgb(249, 251, 231);"></button>
                <button class="color" style="background: rgb(240, 244, 195);"></button>
                <button class="color" style="background: rgb(230, 238, 156);"></button>
                <button class="color" style="background: rgb(220, 231, 117);"></button>
                <button class="color" style="background: rgb(212, 225, 87);"></button>
                <button class="color" style="background: rgb(205, 220, 57);"></button>
                <button class="color" style="background: rgb(192, 202, 51);"></button>
                <button class="color" style="background: rgb(175, 180, 43);"></button>
                <button class="color" style="background: rgb(158, 157, 36);"></button>
                <button class="color" style="background: rgb(130, 119, 23);"></button>
                <button class="color" style="background: rgb(255, 253, 231);"></button>
                <button class="color" style="background: rgb(255, 249, 196);"></button>
                <button class="color" style="background: rgb(255, 245, 157);"></button>
                <button class="color" style="background: rgb(255, 241, 118);"></button>
                <button class="color" style="background: rgb(255, 238, 88);"></button>
                <button class="color" style="background: rgb(255, 235, 59);"></button>
                <button class="color" style="background: rgb(253, 216, 53);"></button>
                <button class="color" style="background: rgb(251, 192, 45);"></button>
                <button class="color" style="background: rgb(249, 168, 37);"></button>
                <button class="color" style="background: rgb(245, 127, 23);"></button>
                <button class="color" style="background: rgb(255, 248, 225);"></button>
                <button class="color" style="background: rgb(255, 236, 179);"></button>
                <button class="color" style="background: rgb(255, 224, 130);"></button>
                <button class="color" style="background: rgb(255, 213, 79);"></button>
                <button class="color" style="background: rgb(255, 202, 40);"></button>
                <button class="color" style="background: rgb(255, 193, 7);"></button>
                <button class="color" style="background: rgb(255, 179, 0);"></button>
                <button class="color" style="background: rgb(255, 160, 0);"></button>
                <button class="color" style="background: rgb(255, 143, 0);"></button>
                <button class="color" style="background: rgb(255, 111, 0);"></button>
                <button class="color" style="background: rgb(255, 243, 224);"></button>
                <button class="color" style="background: rgb(255, 224, 178);"></button>
                <button class="color" style="background: rgb(255, 204, 128);"></button>
                <button class="color" style="background: rgb(255, 183, 77);"></button>
                <button class="color" style="background: rgb(255, 167, 38);"></button>
                <button class="color" style="background: rgb(255, 152, 0);"></button>
                <button class="color" style="background: rgb(251, 140, 0);"></button>
                <button class="color" style="background: rgb(245, 124, 0);"></button>
                <button class="color" style="background: rgb(239, 108, 0);"></button>
                <button class="color" style="background: rgb(230, 81, 0);"></button>
                <button class="color" style="background: rgb(251, 233, 231);"></button>
                <button class="color" style="background: rgb(255, 204, 188);"></button>
                <button class="color" style="background: rgb(255, 171, 145);"></button>
                <button class="color" style="background: rgb(255, 138, 101);"></button>
                <button class="color" style="background: rgb(255, 112, 67);"></button>
                <button class="color" style="background: rgb(255, 87, 34);"></button>
                <button class="color" style="background: rgb(244, 81, 30);"></button>
                <button class="color" style="background: rgb(230, 74, 25);"></button>
                <button class="color" style="background: rgb(216, 67, 21);"></button>
                <button class="color" style="background: rgb(191, 54, 12);"></button>
                <button class="color" style="background: rgb(239, 235, 233);"></button>
                <button class="color" style="background: rgb(215, 204, 200);"></button>
                <button class="color" style="background: rgb(188, 170, 164);"></button>
                <button class="color" style="background: rgb(161, 136, 127);"></button>
                <button class="color" style="background: rgb(141, 110, 99);"></button>
                <button class="color" style="background: rgb(121, 85, 72);"></button>
                <button class="color" style="background: rgb(109, 76, 65);"></button>
                <button class="color" style="background: rgb(93, 64, 55);"></button>
                <button class="color" style="background: rgb(78, 52, 46);"></button>
                <button class="color" style="background: rgb(62, 39, 35);"></button>
                <button class="color" style="background: rgb(250, 250, 250);"></button>
                <button class="color" style="background: rgb(245, 245, 245);"></button>
                <button class="color" style="background: rgb(238, 238, 238);"></button>
                <button class="color" style="background: rgb(224, 224, 224);"></button>
                <button class="color" style="background: rgb(189, 189, 189);"></button>
                <button class="color" style="background: rgb(158, 158, 158);"></button>
                <button class="color" style="background: rgb(117, 117, 117);"></button>
                <button class="color" style="background: rgb(97, 97, 97);"></button>
                <button class="color" style="background: rgb(66, 66, 66);"></button>
                <button class="color" style="background: rgb(33, 33, 33);"></button>
              </div>
			<?php endif; ?>
        </div>
      </div>
    </li>
    <li>
      <div class="content__wrapper">
        <h2 class="text-color">Her</h2>
        
        <img src="http://lewihussey.com/codepen-img/her.jpg">
      </div>
    </li>
    <li>
      <div class="content__wrapper">
        <h2 class="text-color">About</h2>
        
        <p>Created by <a class="text-color" href="http://lewihussey.com" target="_blank">Lewi Hussey</a></p>
      </div>
    </li>
  </ul>
</section>
<script>
  $(document).ready(function () {
    
    // Variables
    var clickedTab = $(".tabs > .active");
    var tabWrapper = $(".tab__content");
    var activeTab = tabWrapper.find(".active");
    var activeTabHeight = activeTab.outerHeight();
    
    // Show tab on page load
    activeTab.show(100);
    
    // Set height of wrapper on page load
    tabWrapper.height(activeTabHeight);
    
    $(".tabs > li").on("click", function () {
      
      // Remove class from active tab
      $(".tabs > li").removeClass("active");
      
      // Add class active to clicked tab
      $(this).addClass("active");
      
      // Update clickedTab variable
      clickedTab = $(".tabs .active");
      
      // fade out active tab
      activeTab.fadeOut(100, function () {
        
        // Remove active class all tabs
        $(".tab__content > li").removeClass("active");
        
        // Get index of clicked tab
        var clickedTabIndex = clickedTab.index();
        
        // Add class active to corresponding tab
        $(".tab__content > li").eq(clickedTabIndex).addClass("active");
        
        // update new active tab
        activeTab = $(".tab__content > .active");
        
        // Update variable
        activeTabHeight = activeTab.outerHeight();
        
        // Animate height of wrapper to new tab height
        tabWrapper.stop().delay(10).animate({
          height: activeTabHeight
        }, 100, function () {
          
          // Fade in active tab
          activeTab.delay(10).fadeIn(100);
          
        });
      });
    });
    
    // Variables
    var colorButton = $(".colors li");
    
    colorButton.on("click", function () {
      
      // Remove class from currently active button
      $(".colors > li").removeClass("active-color");
      
      // Add class active to clicked button
      $(this).addClass("active-color");
      
      // Get background color of clicked
      var newColor = $(this).attr("data-color");
      
      // Change background of everything with class .bg-color
      $(".bg-color").css("background-color", newColor);
      
      // Change color of everything with class .text-color
      $(".text-color").css("color", newColor);
    });
  });
</script>
</body>
</html>