<?php

//region Add theme colors to acf color picker field

function add_theme_colors_to_acf_color_picker() {
  global $theme_color_pallets;
  $theme_color_pallets = is_array( $theme_color_pallets ) && ! empty( $theme_color_pallets ) ? array_keys( $theme_color_pallets ) : array();
  ?>
  <script type="text/javascript">
    (function ($) {
      const color_palettes = <?= json_encode( $theme_color_pallets ); ?>;

      if (color_palettes?.length) {
        acf.add_filter('color_picker_args', function (args, $field) {

          // do something to args
          args.palettes = color_palettes;

          // return
          return args;
        });
      }

    })(jQuery);
  </script>
  <?php
}

add_action( 'acf/input/admin_footer', 'add_theme_colors_to_acf_color_picker' );

//endregion Add theme colors to acf color picker field

//region acf field to pull all custom image sizes
add_filter( 'acf/load_field/name=image_size', 'acf_load_all_custom_image_sizes' );

function acf_load_all_custom_image_sizes( $field ) {
  foreach ( \Theme\Helpers::get_all_image_sizes() as $key => $size ) {
    $field['choices'][ $key ] = $size['width'] . 'x' . $size['height'];
  }

  // return the field
  return $field;
}

//endregion acf field to pull all custom image sizes
