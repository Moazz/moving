import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";


export default async (footer = document) => {
  const media = window.matchMedia('(max-width: 599px)');


  animations(footer);
  imageLazyLoading(footer);
};

