
import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import {stickySidebar} from "../../scripts/general/sticky-sidebar";

gsap.registerPlugin(ScrollTrigger);
const twoColumnsBlock = async (block) => {

  const onThisPage = block.querySelector('.on_this_page')
  const sectionsWrapper = block.querySelector('.sections-wrapper');

  stickySidebar(onThisPage,sectionsWrapper);

  animations(onThisPage);
  imageLazyLoading(onThisPage);
};

export default twoColumnsBlock;

