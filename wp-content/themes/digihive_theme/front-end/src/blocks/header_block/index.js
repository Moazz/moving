import './style.scss';
import {imageLazyLoading} from '../../scripts/functions/imageLazyLoading';
import {animations} from '../../scripts/general/animations';

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
export default async (header) => {

  // region save headerSticky height
  header.classList.add('header-sticky');
  window.headerSticky = header?.getBoundingClientRect()?.height - 2 ?? 0;
  header.classList.remove('header-sticky');
  // endregion save headerSticky height


  animations(header);
  imageLazyLoading(header);
};

