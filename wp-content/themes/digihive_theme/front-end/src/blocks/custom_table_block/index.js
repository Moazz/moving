import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import 'datatables'
import $ from 'jquery';


/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const tableBlock = async (block) => {

  let numberOfRows = block.querySelector('.custom-main-table')?.dataset.pageLength;

  $(block.querySelector('.custom-main-table'))?.DataTable({
    "paging": true,
    "autoWidth": true,
    "searching": false,
    "info": false,
    "bLengthChange": false,
    "iDisplayLength": numberOfRows ?? 10,      //increasing the number of the rows
    "lengthMenu": [[5, 10, 20, -1], [5, 10, 50, "Mostrar Todo"]],
    language: {
      paginate: {
        next: `<svg class="arrows " width="33" height="33" viewBox="0 0 33 33"
         fill="none"
         xmlns="http://www.w3.org/2000/svg">
      <path
        d="M14.25 23C13.9336 23 13.6523 22.8945 13.4414 22.6836C12.9844 22.2617 12.9844 21.5234 13.4414 21.1016L18.2578 16.25L13.4414 11.4336C12.9844 11.0117 12.9844 10.2734 13.4414 9.85156C13.8633 9.39453 14.6016 9.39453 15.0234 9.85156L20.6484 15.4766C21.1055 15.8984 21.1055 16.6367 20.6484 17.0586L15.0234 22.6836C14.8125 22.8945 14.5312 23 14.25 23Z"
        fill="#184B9B"/>
      <circle cx="16.5" cy="16.5" r="15.5" stroke="#184B9B"
              stroke-width="2"/>
    </svg>`,
        previous: `<svg class="arrows"  width="33" height="33" viewBox="0 0 33 33" fill="none"
             xmlns="http://www.w3.org/2000/svg">
          <path
            d="M18.75 9C19.0664 9 19.3477 9.10547 19.5586 9.31641C20.0156 9.73828 20.0156 10.4766 19.5586 10.8984L14.7422 15.75L19.5586 20.5664C20.0156 20.9883 20.0156 21.7266 19.5586 22.1484C19.1367 22.6055 18.3984 22.6055 17.9766 22.1484L12.3516 16.5234C11.8945 16.1016 11.8945 15.3633 12.3516 14.9414L17.9766 9.31641C18.1875 9.10547 18.4688 9 18.75 9Z"
            fill="#184B9B"/>
          <circle cx="16.5" cy="16.5" r="15.5" stroke="#184B9B"
                  stroke-width="2"/>
        </svg>`,
      }
    },
    "fnDrawCallback": function (oSettings) {
      var pgr = $(oSettings.nTableWrapper).find('.dataTables_paginate')
      if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
        pgr.hide();
      } else {
        pgr.show()
      }
    }
  });

  animations(block);
  imageLazyLoading(block);
};

export default tableBlock;

