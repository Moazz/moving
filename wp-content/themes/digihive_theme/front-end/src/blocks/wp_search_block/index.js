import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";

/**
 *
 * @param block {HTMLElement}
 * @returns {Promise<void>}
 */
const wpSearchBlock = async (block) => {

  // add block code here


    animations(block);
    imageLazyLoading(block);
};

export default wpSearchBlock;

