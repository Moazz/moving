acf_register_block_type(array(
  'name'                   => '{{snakeCase name}}',
  'title'                  => __('{{titleCase name}}'),
  'template_directory_uri' => get_template_directory_uri(),
  'render_template'        => 'template-parts/blocks/{{snakeCase name}}/index.php',
  'category'               => '{{kebabCase domain}}-blocks',
  'icon'                   => 'admin-appearance',
  'supports'               => array('anchor' => true),
  'mode'                   => 'edit',
  'example'                => array(
    'attributes' => array(
      'mode' => 'preview',
      'data' => array('is_screenshot' => true),
    )
  )
));
