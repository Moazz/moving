<?php
/*
 * Template Name: Full Width With Hero
 * */

get_header();
?>

<?php while ( have_posts() ) : the_post(); ?>
  <div class="full-width-template-with-hero">
    <?php get_template_part( "blocks/hero_block/single-hero" ); ?>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
  </div>
<?php endwhile;

get_footer();
