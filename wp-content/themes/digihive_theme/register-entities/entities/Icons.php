<?php

namespace Register_Entities;

class Icons extends Entity {
  public static function init() {

    register_post_type( 'icons', [
      'labels'              => [
        'name'               => _x( 'Icons', 'Post Type General Name', 'digihive_theme' ),
        'singular_name'      => _x( 'Icon', 'Post Type Singular Name', 'digihive_theme' ),
        'menu_name'          => __( 'Icons', 'digihive_theme' ),
        'parent_item_colon'  => __( 'Parent Icon', 'digihive_theme' ),
        'all_items'          => __( 'All Icons', 'digihive_theme' ),
        'view_item'          => __( 'View Icon', 'digihive_theme' ),
        'add_new_item'       => __( 'Add New Icon', 'digihive_theme' ),
        'add_new'            => __( 'Add New', 'digihive_theme' ),
        'edit_item'          => __( 'Edit Icon', 'digihive_theme' ),
        'update_item'        => __( 'Update Icon', 'digihive_theme' ),
        'search_items'       => __( 'Search Icon', 'digihive_theme' ),
        'not_found'          => __( 'Not Found', 'digihive_theme' ),
        'not_found_in_trash' => __( 'Not found in Trash', 'digihive_theme' ),
      ],
      'label'               => __( 'icons', 'digihive_theme' ),
      'description'         => __( 'Icon news and reviews', 'digihive_theme' ),
      // Features this CPT supports in Post Editor
      'supports'            => array(
        'title',
        'custom-fields',
      ),
      // You can associate this CPT with a taxonomy or custom taxonomy.
      /* A hierarchical CPT is like Pages and can have
    * Parent and child items. A non-hierarchical CPT
    * is like Posts.
    */
      'hierarchical'        => false,
      'public'              => true,
      'show_ui'             => true,
      'menu_icon'           => 'dashicons-visibility',
      'show_in_menu'        => true,
      'show_in_nav_menus'   => true,
      'show_in_admin_bar'   => true,
      'menu_position'       => 5,
      'can_export'          => true,
      'has_archive'         => false,
      'exclude_from_search' => true,
      'publicly_queryable'  => true,
      'capability_type'     => 'post',
      'show_in_rest'        => true,
    ] );

  }
}

