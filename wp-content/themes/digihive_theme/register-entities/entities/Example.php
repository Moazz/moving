<?php

namespace Register_Entities;

class Example extends Entity {
  public static function init() {
    register_post_type( 'example', [
      'labels'             => [
        'name'               => 'Example',
        'singular_name'      => 'Example',
        'add_new'            => 'Add new',
        'add_new_item'       => 'Add new Example',
        'edit_item'          => 'Edit Example',
        'new_item'           => 'New Example',
        'view_item'          => 'View Example',
        'search_items'       => 'Find Example',
        'not_found'          => 'Example not found',
        'not_found_in_trash' => 'Example not found in trash',
        'parent_item_colon'  => '',
        'menu_name'          => 'Example'
      ],
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => [ 'slug' => 'example', 'with_front' => false ],
      'capability_type'    => 'post',
      'has_archive'        => false,
      'hierarchical'       => false,
      'custom-fields'      => true,
      'menu_position'      => 3,
      'taxonomies'         => array( 'category' ),
      'supports'           => [
        'title',
        'editor',
        'author',
        'thumbnail',
        'excerpt',
        'tags',
        'category'
      ]
    ] );

    register_taxonomy(
      'guide_urls_structure',
      'example',
      array(
        'label'        => __( 'Example URLS Structure' ),
        'rewrite'      => [ 'slug' => 'example', 'hierarchical' => true ],
        'hierarchical' => true,
        'description'  => 'Used to define the URLS Structure of Example'
      )
    );

  }
}

