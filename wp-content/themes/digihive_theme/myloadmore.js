jQuery(function ($) {
  let canBeLoaded = true;
  window.ajax_load_more = function(){
    const element = document.querySelector('.ajax-posts');
    if (!element) {
      return
    }

    const data = {
      'action': 'loadmore',
      'query': element.dataset.posts,
      'page': element.dataset.currentPage
    };
    if(element.getBoundingClientRect().bottom < 800 && canBeLoaded ){
      canBeLoaded = false;
      let currentPage = element.dataset.currentPage
      $.ajax({
        url: _loadmore_params.ajaxurl, // AJAX handler
        data: data,
        type: 'POST',
        beforeSend: function( xhr ){
          canBeLoaded = false;
          $('.posts-loader').fadeIn();
        },
        success:function(data){
          if( data ) {
            $('.ajax-posts').append(data);
            setTimeout(function (){
              $('.posts-loader').fadeOut();
              canBeLoaded = true; // the ajax is completed, now we can run it again
            },500)
            element.dataset.currentPage = `${currentPage+=1}`;
            if (currentPage === element.dataset.maxPages) {
              $('.posts-loader').fadeOut();
            }
          }else{
            $('.posts-loader').fadeOut();
          }
        }
      });
    }
  };
});
