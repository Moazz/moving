<?php
// Create id attribute allowing for custom "anchor" value.
$id = 'single-hero-block';
// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'hero_block';
$className = 'hero_block ';


/****************************
 *     Custom ACF Meta      *
 *
 *
 *
 ****************************/
$title   = get_the_title();
$excerpt = get_the_excerpt();
?>
<!-- region digihive_theme's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<?php if ( $title ): ?>
  <h1><?= $title ?></h1>
<?php endif; ?>
<?php if ( $excerpt ): ?>
  <div><?= $excerpt ?></div>
<?php endif ?>
<?php if ( has_post_thumbnail() ) {
  echo \Theme\Helpers::get_post_image( get_the_ID(), 'large' );
} ?>
</section>


<!-- endregion digihive_theme's Block -->
