<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'two_columns_block';
$className = 'two_columns_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/blocks/two_columns_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$on_this_page = get_field( 'title' );
?>
<!-- region on digihive_theme's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <div class="two-columns-wrapper">
    <?php if ( ! $is_preview ) { ?>
      <div class="on_this_page">
        <?php if ( $on_this_page ) { ?>
          <h4 class="headline-4"><?= $on_this_page ?></h4>
        <?php } ?>
        <?php if ( have_rows( 'in_this_section' ) ) { ?>
          <ul class="in-this-section">
            <?php while ( have_rows( 'in_this_section' ) ) {
              the_row();
              $text       = get_sub_field( 'text' );
              $link_or_id = get_sub_field( 'link_or_id' );
              $location   = get_sub_field( 'location' );
              $link       = get_sub_field( 'link' );
              if ( $link_or_id === 'link' ) {
                $href = $link;
              } else {
                $href = '#' . $location;
              }
              ?>
              <?php if ( $text ) { ?>
                <li class="in-this-section-link">
                  <a class="in-this-section-a" href="<?= $href ?>"><?= $text ?></a>
                </li>
              <?php } ?>
            <?php } ?>
          </ul>
        <?php } ?>
      </div>
    <?php } ?>
    <div class="sections-wrapper entry-content">
      <InnerBlocks/>
    </div>
  </div>
</div>
</section>


<!-- endregion on digihive_theme's Block -->
