<?php
$post_id        = @$args['post_id'] ?: get_the_ID();
$post_title     = get_the_title( $post_id );
$post_permalink = get_the_permalink( $post_id );
?>
<div id="post-id-<?= $post_id ?>" class="col iv-st-from-bottom">
  <div class="news-card">
    <a href="<?= $post_permalink ?>" class="news-card-image aspect-ratio">
      <?= get_post_lazyloaded_image( $post_id, 'medium' ) ?>
    </a>
  </div>
</div>
