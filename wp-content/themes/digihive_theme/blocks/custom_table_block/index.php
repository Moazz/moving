<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'custom_table_block';
$className = 'custom_table_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/blocks/custom_table_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title               = get_field( 'title' );
$cta_link            = get_field( 'cta_link' );
$table               = get_field( 'table' );
$table_rows_per_page = get_field( 'table_rows_per_page' );
?>
<!-- region digihive_theme's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <?php if ( $title ) { ?>
    <div class="custom-table-wrapper">
      <p class="paragraph"><?= $title ?></p>
      <?php if ( \Theme\Helpers::get_key_from_array( 'url', $cta_link ) ) { ?>
        <a class="theme-cta-link" href="<?= $cta_link['url'] ?>" target="<?= $cta_link['target'] ?: '_self' ?>"><?= $cta_link['title'] ?></a>
      <?php } ?>
    </div>
  <?php } ?>
  <?php
  if ( ! empty ( $table ) ) {

  echo '<table class="custom-main-table" data-page-length="' . $table_rows_per_page . '">';

  if ( ! empty( $table['caption'] ) ) {

    echo '<caption>' . $table['caption'] . '</caption>';
  }

  if ( ! empty( $table['header'] ) ) {

    echo '<thead>';

    echo '<tr class="table-header has-arrow">';

    foreach ( $table['header'] as $th ) {

      echo '<th>';
      echo $th['c'];
      echo '</th>';
    }

    echo '</tr>';

    echo '</thead>';
  }

  echo '<tbody>';

  foreach ( $table['body'] as $tr ) {

  echo '<tr class="table-body">';

  foreach ( $tr

  as  $key => $td ) { ?>

  <td data-label="<?php if ( ! empty( $table['header'] ) ) {
    echo $table['header'][ $key ]['c'];
  } ?>">
    <?php echo $td['c'];
    echo '</td>';
    }

    echo '</tr>';
    }

    echo '</tbody>';

    echo '</table>';
    } ?>
</div>
</section>


<!-- endregion digihive_theme's Block -->
