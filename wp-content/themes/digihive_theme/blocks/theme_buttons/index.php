<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'theme_buttons';
$className = 'theme_buttons';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/blocks/theme_buttons/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
global $base_size;
$buttons_gap       = get_field( 'buttons_gap' );
$alignment_group   = get_field( 'alignment' );
$alignment         = \Theme\Helpers::get_key_from_array( 'alignment', $alignment_group );
$column            = \Theme\Helpers::get_key_from_array( 'column', $buttons_gap );
$row               = \Theme\Helpers::get_key_from_array( 'row', $buttons_gap );
$column_style      = $column ? 'column-gap:' . intval( $column ) / $base_size . 'rem;' : '';
$row_style         = $row ? 'row-gap:' . intval( $row ) / $base_size . 'rem;' : '';
$alignment_style   = $alignment ? 'justify-content:' . $alignment . ';' : '';
$buttons_gap_style = ( $column_style || $row_style || $alignment_style ) ? "style='$column_style $row_style $alignment_style'" : '';
// alignment
?>
<!-- region digihive_theme's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <?php if ( have_rows( 'theme_buttons' ) ) {
    ?>
    <div class="digihive_theme-buttons" <?= $buttons_gap_style ?>>
      <?php while ( have_rows( 'theme_buttons' ) ) {
        the_row();
        $custom_button  = get_sub_field( 'custom_button' );
        $buttons_themes = get_sub_field( 'buttons_themes' );
        if ( ! $custom_button ) {
          switch ( $buttons_themes ) {
            case "button-2":
              get_template_part( "blocks/theme_buttons/button-templates/btn-2" );
              break;
            case "button-3":
              get_template_part( "blocks/theme_buttons/button-templates/btn-3" );
              break;
            case "button-4":
              get_template_part( "blocks/theme_buttons/button-templates/btn-4" );
              break;
            case "button-5":
              get_template_part( "blocks/theme_buttons/button-templates/btn-5" );
              break;
            case "button-6":
              get_template_part( "blocks/theme_buttons/button-templates/btn-6" );
              break;
            default:
              get_template_part( "blocks/theme_buttons/button-templates/btn-1" );
          }
        } else {
          $cta_url    = get_sub_field( 'cta_url' );
          $cta_text   = get_sub_field( 'cta_text' );
          $cta_target = get_sub_field( 'cta_target' );
          $margins    = get_sub_field( 'margins' );
          // region Styles settings
          $styles = get_sub_field( 'styles' );
          // endregion Styles settings
          // region Typography settings
          $font_size = get_sub_field( 'font_size' );
          // endregion Typography settings
          // region color settings
          $text_color       = get_sub_field( 'text_color' );
          $background_color = get_sub_field( 'background_color' );
          // endregion color settings
          // region border settings
          $border_radius = get_sub_field( 'border_radius' );
          // endregion border settings
          // region width settings
          $width_settings = get_sub_field( 'width_settings' );
          // endregion width settings  ?
          // region options settings
          $has_arrow = get_sub_field( 'has_arrow' );
          // endregion options settings  ?
          $mt            = \Theme\Helpers::get_key_from_array( 'top', $margins ) ? 'margin-top:' . intval( $margins['top'] ) / $base_size . 'rem;' : '';
          $mb            = \Theme\Helpers::get_key_from_array( 'bottom', $margins ) ? 'margin-bottom:' . intval( $margins['bottom'] ) / $base_size . 'rem;' : '';
          $bg            = $background_color ? 'background-color:' . $background_color . ';' : '';
          $color         = $text_color ? 'color:' . $text_color . ';' : '';
          $border_radius = $border_radius ? 'border-radius:' . $border_radius . 'px;' : '';
          $width         = $width_settings ? 'width:' . $width_settings . ';' : '';
          $typography    = $font_size ? 'font-size:' . intval( $font_size ) / $base_size . 'rem;' : '';
          $style         = $mt || $mb || $text_color || $background_color || $border_radius || $width || $typography ? "style='$mt $mb $bg $color $border_radius $width $typography'" : '';
          ?>
          <a href="<?= $cta_url ?>" class="digihive_theme-button <?= $styles ?> <?= $has_arrow ? 'digihive_theme-button-has-arrow' : '' ?>" target="<?= $cta_target ?>" <?= $style ?>><?= $cta_text ?>
            <?php if ( $has_arrow ) { ?>
              <svg class="digihive_theme-button-arrow" width="9" height="14" viewBox="0 0 9 14" fill="none">
                <path d="M1.03906 13L7.03906 7L1.03906 1" stroke="#0A0A0A"
                      stroke-width="2" stroke-linecap="round"
                      stroke-linejoin="round"/>
              </svg>
            <?php } ?>
          </a>
        <?php }
      } ?>
    </div>
  <?php } ?>
</div>
</section>

<!-- endregion digihive_theme's Block -->
