<?php
global $base_size;
$url     = ! isset( $type ) ? get_sub_field( 'cta_url' ) : $url;
$text    = ! isset( $type ) ? get_sub_field( 'cta_text' ) : $text;
$target  = ! isset( $type ) ? get_sub_field( 'cta_target' ) : $target;
$margins = get_sub_field( 'margins' );
$mt      = ! isset( $cta_mt ) ? ( \Theme\Helpers::get_key_from_array( 'top', $margins ) ? 'margin-top:' . intval( $margins['top'] ) / $base_size . 'rem;' : '' ) : ($cta_mt ? 'margin-top:' . intval( $cta_mt ) / $base_size . 'rem;' : '');
$mb      = ! isset( $cta_mt ) ? ( \Theme\Helpers::get_key_from_array( 'top', $margins ) ? 'margin-bottom:' . intval( $margins['top'] ) / $base_size . 'rem;' : '' ) : ($cta_mt ? 'margin-bottom:' . intval( $cta_mt ) / $base_size . 'rem;' : '');
$style   = ( $mt || $mb ) ? "style='$mt $mb'" : '';
echo '
<a class="theme-cta-button"
target="' . $target . '"
href="' . $url . '" ' . $style . '>' . $text . '</a>';
