<?php wp_footer(); ?>
<!--Footer ACF-->
<?php
$code_before_end_of_body_tag = get_field( 'code_before_end_of_body_tag', 'options' );
?>
<!--region footer-->
<!-- remove footer if page template if full with no header and footer-->
<?php if ( ! is_page_template( 'templates/full-width-no-header-footer.php' ) ): ?>
  <footer class="digihive-theme-footer" style="height: 500px;background: red"></footer>
<?php endif; ?>
</main>
<!-- General Custom Modal-->
<div class="custom-modal" id="custom-modal">
  <div class="custom-modal-inner">
    <button class="close-modal" aria-label="Close Card Modal">
      <svg class="close-modal-text" x="0px" y="0px" width="20" height="20"
           viewBox="0 0 512.001 512.001">
        <path d="M284.286,256.002L506.143,34.144c7.811-7.811,7.811-20.475,0-28.285c-7.811-7.81-20.475-7.811-28.285,0L256,227.717
L34.143,5.859c-7.811-7.811-20.475-7.811-28.285,0c-7.81,7.811-7.811,20.475,0,28.285l221.857,221.857L5.858,477.859
c-7.811,7.811-7.811,20.475,0,28.285c3.905,3.905,9.024,5.857,14.143,5.857c5.119,0,10.237-1.952,14.143-5.857L256,284.287
l221.857,221.857c3.905,3.905,9.024,5.857,14.143,5.857s10.237-1.952,14.143-5.857c7.811-7.811,7.811-20.475,0-28.285
L284.286,256.002z" fill="currentColor"/>
      </svg>
      <svg class="close-modal-video" width="20" height="20" viewBox="0 0 20 20" fill="none">
        <rect x="0.5" y="0.5" width="19" height="19" stroke="white"/>
        <line x1="1.00785" y1="0.95797" x2="18.8733" y2="18.8234" stroke="white"/>
        <line x1="18.7129" y1="1.66508" x2="0.84748" y2="19.5305" stroke="white"/>
      </svg>
      <span class="contact-form-title">Get in touch</span>
    </button>
    <div class="custom-modal-content">
    </div>
  </div>
</div>
<?= $code_before_end_of_body_tag ?>
</body>
</html>
