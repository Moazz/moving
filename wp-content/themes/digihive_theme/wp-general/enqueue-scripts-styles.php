<?php

add_action( 'get_footer', 'digihive_theme_scripts' );
function digihive_theme_scripts() {

  if ( file_exists( get_template_directory() . '/assets/manifest.json' ) ) {
    $asset_map    = json_decode(
      file_get_contents( get_template_directory() . '/assets/manifest.json' ),
      true
    );
    $asset_map_js = array_filter( $asset_map, function ( $path ) {
      return pathinfo( $path )['extension'] == 'js';
    } );
    foreach ( $asset_map_js as $key => $path ) {
      wp_enqueue_script( $key, $path, [], null, true );
    }

    $asset_map_css = array_filter( $asset_map, function ( $path ) {
      return pathinfo( $path )['extension'] == 'css';
    } );
    foreach ( $asset_map_css as $key => $path ) {
      wp_enqueue_style( $key, $path, [], null );
    }

  } else {
    wp_enqueue_script( 'main.js', get_template_directory_uri() . '/assets/main.js', null, time(), true );
    wp_enqueue_style( 'main.css', get_template_directory_uri() . '/assets/main.css', null, time() );
  }

}

function digihive_theme_localize_scripts() { ?>
  <script type="text/javascript" id="theme_objects">
    /* <![CDATA[ */
    var theme_ajax_object = {
      "ajax_url": "<?=admin_url( 'admin-ajax.php' )?>",
      "_ajax_nonce": "<?=wp_create_nonce( 'nonce_ajax_more_posts' )?>"
    };
    /* ]]> */
    var site_url_object = {
      "site_url": "<?=site_url()?>"
    }
  </script>
  <?php
}

add_action( 'wp_head', 'digihive_theme_localize_scripts' );

add_action( 'admin_head', 'localize_site_url_in_admin' );
function localize_site_url_in_admin() {
  ?>
  <script id="site_url_in_admin">
    var siteurl = "<?=site_url()?>"
  </script>
  <?php
}
