<?php

//region ACF show Options & Settings in Dashboard

// (Optional) Hide the ACF admin menu item.
add_filter( 'acf/settings/show_admin', 'my_acf_settings_show_admin' );
function my_acf_settings_show_admin( $show_admin ) {
  return true;
}

if ( function_exists( 'acf_add_options_page' ) ) {
  acf_add_options_page();
}

//endregion ACF show Options & Settings in Dashboard

// region Add global settings and styles to admin menu

add_action( 'admin_menu', 'register_global_settings_and_styles' );
function register_global_settings_and_styles() {
  add_menu_page( 'Global Settings & Styles', 'Global Settings', 'manage_options', site_url() . '/global-settings.php', '', 'dashicons-admin-generic', 63.3 );
}

// endregion add global settings and styles to admin menu


