<?php

//region custom image sizes

$image_dimensions_increased = 50;

function custom_image_sizes() {
  global $image_dimensions_increased;
  /* Please follow the following template to build all image sizes
  /*  add_image_size('img-48-48', 48 + $image_dimensions_increased, 48 + $image_dimensions_increased);
  image naming configuration
    ***** Round numbers to the nearest number up
    width = image width
    height =  image height
  */


  // region  sizes between 100
  add_image_size( 'img-48-48', 48 + $image_dimensions_increased, 48 + $image_dimensions_increased );
  add_image_size( 'img-99-99', 99 + $image_dimensions_increased, 99 + $image_dimensions_increased );
  // endregion  sizes between 100

  //  region sizes between  200

  //  endregion sizes between  200

  //  region sizes between  300

  //  endregion sizes between  300

  //  region sizes between  400

  //  region sizes between  500

  //  endregion sizes between  500

  //  region sizes between  600

  //  endregion sizes between  600
}

custom_image_sizes();

//endregion custom image sizes

