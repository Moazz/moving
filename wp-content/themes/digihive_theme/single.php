<?php
get_header();
global $post;
$post_id      = get_the_ID();
$author_id    = $post->post_author;
$thumbnail_id = get_post_thumbnail_id();
$alt          = get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true );
$author_name  = get_the_author_meta( 'display_name', $author_id );;
$author_url = get_author_posts_url( $author_id );
$excerpt    = get_the_excerpt( $post_id );
$title      = get_field( 'title', $post_id );

?>
<?php if ( have_posts() ): the_post(); ?>
  <div class="single-post-wrapper">
    <div class="container">
        <div class="entry-content">
          <?php the_content(); ?>
        </div>
    </div>
  </div>
<?php endif; ?>
<?php
get_footer();

