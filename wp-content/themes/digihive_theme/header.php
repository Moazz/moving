<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="description" content="<?php if ( is_single() ) {
    single_post_title( '', true );
  } else {
    bloginfo( 'name' );
    echo " - ";
    bloginfo( 'description' );
  } ?>"/>
  <meta
    content="width=device-width, initial-scale=1.0, maximum-scale=5, minimum-scale=1.0"
    name="viewport">
  <meta content="ie=edge" http-equiv="X-UA-Compatible">
  <!-- fix container-->
  <style>
    html {

      font-size: calc((100vw / 375) * 10);
      width: 100vw;
      overflow-x: hidden;
      scrollbar-color: rgb(90, 90, 90) rgba(0, 0, 0, 0.2);
      scrollbar-width: thin;
    }

    html.modal-opened {
      overflow: hidden;
    }

    @supports (-moz-appearance:none) {
      @media screen and (min-width: 992px)  {
        html {
          width: calc(100vw - 8px);
        }
      }
    }

    @media screen and (min-width: 375px) {
      html {
        font-size: 10px;
      }
    }

    @media screen and (min-width: 600px) {
      html {
        font-size: calc((100vw / 992) * 10);
      }
    }

    @media screen and (min-width: 992px) {
      html {
        font-size: calc((100vw / 1440) * 10);
      }
    }

    @media screen and (min-width: 1920px) {
      html {
        font-size: 10px;
      }
    }
  </style>
  <!-- Third party code ACF-->
  <?php
  $code_in_head_tag                    = get_field( 'code_in_head_tag', 'options' );
  $code_before_body_tag_after_head_tag = get_field( 'code_before_body_tag_after_head_tag', 'options' );
  $code_after_body_tag                 = get_field( 'code_after_body_tag', 'options' );
  ?>
  <?php wp_head(); ?>
  <?= $code_in_head_tag ?>
</head>
<?php flush(); ?>
<?= $code_before_body_tag_after_head_tag ?>
<!--preloader style-->
<style>
  body:not(.loaded) {
    opacity: 0;
  }

  body:not(.loaded) * {
    transition: none !important;
  }

  body {
    transition: opacity .5s;
  }

  [modal-content] {
    display: none !important;
  }
</style>
<!--end preloader style-->
<!-- ACF Fields -->
<!-- END ACF -->
<body <?php body_class(); ?>>
<a skip-to-main-content href="#main-content"> <?= __( 'Skip to main content', 'digihive_theme' ) ?></a>
<?= $code_after_body_tag ?>
<!-- remove header if page template if full with no header and footer-->
<?php if ( ! is_page_template( 'templates/full-width-no-header-footer.php' ) ): ?>
  <header class="digihive-theme-header" style="height: 100px;background: red"></header>
<?php endif; ?>
<main id="main-content" class="wp-site-blocks">
