<?php
/**
 * Registration logic for the new ACF field type.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

add_action( 'init', 'my_new_field_include_acf_field_my_new_field' );
/**
 * Registers the ACF field type.
 */
function my_new_field_include_acf_field_my_new_field() {
	if ( ! function_exists( 'acf_register_field_type' ) ) {
		return;
	}

	require_once __DIR__ . '/class-my-new-field-acf-field-my-new-field.php';

	acf_register_field_type( 'my_new_field_acf_field_my_new_field' );
}
